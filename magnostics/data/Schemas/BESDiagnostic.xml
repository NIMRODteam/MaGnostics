<?xml version="1.0" encoding="UTF-8"?>
   
<Block name="BESDiagnostic">

<Description> The Beam Emission Spectroscopy Diagnostic (BES) measures localized, long-wavelength (k_perp*rho_I less than 1)  density fluctuations at high spatial resolution in the core, pedestal, edge and Scrape-Off-Layer regions of DIII-D plasmas in an effort to characterize plasma turbulence and resulting turbulent-driven transport. Fluctuations in the plasma density, temperature, and electrostatic potential are widely recognized to result in the anomalously high cross-field particle and energy diffusion observed in nearly all magnetically confined fusion plasmas. An understanding of the underlying physical mechanisms that give rise to such fluctuations and transport may lead to accurate prediction of their effects in large reactor-scale devices and potentially methods to mitigate this. BES is used to measured such turbulence parameters as: Density Fluctuation Spectra and Amplitudes, Radial and Poloidal Correlation Lengths, Decorrelation Times, Wavenumber Spectra, S(kr,k), Equilibrium Poloidal Turbulence Flows, Fluctuating Poloidal Flows (as pertaining to zonal flows and Geodesic Acoustic Modes), 2D Images and Movies of Turbulence, 2D time-resolved turbulence velocity mapping, Mode-mode coupling, Linear growth rates, Internal Energy Transfer.  The localized density fluctuation measurements are also useful for diagnosing Energetic-Particle-Driven Modes (e.g., TAE, RSAE, BAAE, e-GAM), MHD (NTM), pedestal and ELM structures, as well as scrape-off-layer phenomena.</Description>

  <SingleChoice name="machine" minOccurs="1" maxOccurs="1">
    <Description>The machine to which this diagnostic schema applies.</Description>
    <Choice name="diii-d"/>
	  <Choice name="cmod"/>
	  <Choice name="jet"/>
	  <Choice name="iter"/>
	  <Choice name="nstx"/>
  </SingleChoice>
  
  <RealValue name="lowerFrequency" minOccurs="0">
    <Description>The lower frequency of the range of frequencies that picked up by this diagnostic in kHz.</Description>
  </RealValue>
  <RealValue name="upperFrequency" minOccurs="0">
    <Description>The upper frequency of the range of frequencies that picked up by this diagnostic in kHz.</Description>
  </RealValue>
  <RealValue name="timeWindow" minOccurs="0">
    <Description>The time in seconds to full essentially full discharge.</Description>
  </RealValue>
  <RealValue name="lowerSpacialResolutions" minOccurs="0">
    <Description>The lower limit of lengths of wavelengths resolved by the diagnostic in meters.</Description>
  </RealValue>
  <RealValue name="upperSpacialResolutions" minOccurs="0">
    <Description>The upper limit of lengths of wavelengths resolved by the diagnostic in meters.</Description>
  </RealValue>
  <RealValue name="wavenumberSensitivity" minOccurs="0">
    <Description>The wavenumber below which waves are resolved in cm-1.</Description>
  </RealValue>
  <RealValue name="lowerAccessibleRegion" minOccurs="0">
    <Description>The lower limit of the range of radial poisitions in centimeters that are in view of this diagnostic.</Description>
  </RealValue>

  <RealValue name="upperAccessibleRegion" minOccurs="0">
    <Description>The upper limit of the range of radial poisitions in centimeters that are in view of this diagnostic.</Description>
  </RealValue>	
  <Block name="Array">
    <Description>The linear collection of Beam Emission Spectroscopy Diagnostic (BES) points</Description>
	<IntegerValue name="nChannels" minOccurs="1" maxOccurs="1">
      <Description>The number of channels in the array.</Description>
    </IntegerValue>
	<IntegerValue name="nRChannels" minOccurs="1" maxOccurs="1">
      <Description>The number of radial channels in the array.</Description>
    </IntegerValue>
	<IntegerValue name="nZChannels" minOccurs="1" maxOccurs="1">
      <Description>The number of z-aligned channels in the array.</Description>
    </IntegerValue>
    <RealValue name="dR" minOccurs="1">
      <Description>The z? distance between the radial channels in meters.</Description>
    </RealValue>
    <RealValue name="dZ" minOccurs="1">
      <Description>The radial distance between the z channels in meters.</Description>
    </RealValue>
  </Block>
</Block>