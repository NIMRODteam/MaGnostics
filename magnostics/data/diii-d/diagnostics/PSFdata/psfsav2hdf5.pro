pro psfsav2hdf5

RESTORE, "psf.sav"

;IDL> help,psf,/st      
;** Structure <2057c04>, 6 tags, length=134224, data length=134222, refs=1:
;   PSF             FLOAT     Array[256, 128]
;   R               FLOAT     Array[256]
;   P               FLOAT     Array[128]
;   PSF_R           FLOAT     Array[256]
;   PSF_P           FLOAT     Array[128]
;   STATS           STRUCT    -> <Anonymous> Array[1]

fid = H5F_CREATE("psf.h5")

float_dtid = H5T_IDL_CREATE(psf.psf)

dsid = H5S_CREATE_SIMPLE([256,128])
did = H5D_CREATE(fid,"psf",float_dtid,dsid)
H5D_WRITE, did, psf.psf
H5D_CLOSE, did
H5S_CLOSE, dsid

dsid = H5S_CREATE_SIMPLE([256])
did = H5D_CREATE(fid,"r",float_dtid,dsid)
H5D_WRITE, did, psf.r
H5D_CLOSE, did
H5S_CLOSE, dsid

dsid = H5S_CREATE_SIMPLE([128])
did = H5D_CREATE(fid,"p",float_dtid,dsid)
H5D_WRITE, did, psf.p
H5D_CLOSE, did
H5S_CLOSE, dsid

dsid = H5S_CREATE_SIMPLE([256])
did = H5D_CREATE(fid,"psf_r",float_dtid,dsid)
H5D_WRITE, did, psf.psf_r
H5D_CLOSE, did
H5S_CLOSE, dsid

dsid = H5S_CREATE_SIMPLE([128])
did = H5D_CREATE(fid,"psf_p",float_dtid,dsid)
H5D_WRITE, did, psf.psf_p
H5D_CLOSE, did
H5S_CLOSE, dsid

;IDL> help,psf.stats,/st
;** Structure <2057804>, 18 tags, length=80, data length=78, refs=2:
;   R               FLOAT           196.970
;   Z               FLOAT         -0.871829
;   BEAM            INT              1
;   BEAM_NAME       STRING    'LEFT'
;   BEAME           FLOAT           80.0186
;   MAG             FLOAT           2.66144
;   THETA_PITCH     FLOAT           2.69262
;   TAU             FLOAT       1.67949e-09
;   RAVG            FLOAT           197.378
;   PAVG            FLOAT         -0.868496
;   RFWHM           FLOAT           3.28949
;   PFWHM           FLOAT           1.11760
;   REFOLD          FLOAT           3.90150
;   PEFOLD          FLOAT           1.21920
;   RFWHM_FRAC      FLOAT          0.771415
;   PFWHM_FRAC      FLOAT          0.824325
;   REFOLD_FRAC     FLOAT          0.846741
;   PEFOLD_FRAC     FLOAT          0.859834

gid = H5G_CREATE(fid,"stats")

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"r",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.r
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"z",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.z
H5A_CLOSE, aid
H5S_CLOSE, dsid

int_dtid = H5T_IDL_CREATE(psf.stats.beam)
dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"beam",int_dtid,dsid)
H5A_WRITE, aid, psf.stats.beam
H5A_CLOSE, aid
H5S_CLOSE, dsid
H5T_CLOSE, int_dtid

str_dtid = H5T_IDL_CREATE(psf.stats.beam_name)
dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"beam_name",str_dtid,dsid)
H5A_WRITE, aid, psf.stats.beam_name
H5A_CLOSE, aid
H5S_CLOSE, dsid
H5T_CLOSE, str_dtid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"beame",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.beame
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"mag",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.mag
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"theta_pitch",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.theta_pitch
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"tau",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.tau
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"ravg",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.ravg
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"pavg",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.pavg
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"rfwhm",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.rfwhm
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"pfwhm",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.pfwhm
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"refold",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.refold
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"pefold",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.pefold
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"rfwhm_frac",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.rfwhm_frac
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"pfwhm_frac",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.pfwhm_frac
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"refold_frac",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.refold_frac
H5A_CLOSE, aid
H5S_CLOSE, dsid

dsid = H5S_CREATE_SCALAR()
aid = H5A_CREATE(gid,"pefold_frac",float_dtid,dsid)
H5A_WRITE, aid, psf.stats.pefold_frac
H5A_CLOSE, aid
H5S_CLOSE, dsid

H5G_CLOSE, gid

H5T_CLOSE, float_dtid
H5F_CLOSE, fid

; or write the entire structure out at once

fid = H5F_CREATE("psf2.h5")
struc_dtid = H5T_IDL_CREATE(psf)
dsid = H5S_CREATE_SIMPLE(1)
did = H5D_CREATE(fid, 'psf', struc_dtid, dsid)
H5D_WRITE, did, psf

H5D_CLOSE, did
H5S_CLOSE, dsid
H5T_CLOSE, struc_dtid
H5F_CLOSE, fid

; To read in the second case in idl, use

;s = H5_PARSE("psf2.h5",/READ_DATA)
;psf = s.psf._data
;HELP, psf, /STRUC

end

