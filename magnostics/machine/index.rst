

Machine data
~~~~~~~~~~~~~~

For each diagnostic, it is important to be able to place the geometry in context
of the overall machine.  Before getting into the details of the geometry for
each diagnostic, it is useful just to understand the basic geometry of each
experimental machine.  

The simplest description is the "Simple Wall".  This is an axisymmetric, closed
loop describing approximately the bound of the plasma.  It does not include such
details as ports, antennas, ...

Here we review the basic example of a simple wall and associated fileds
