#!/usr/bin/env python
"""
Author: Travis Austin
Date: 12/15/2009

Purpose: Plotting Vorpal date in R/Z/Phi Coordinates

./runPlotStrapSource.py myfile.h5 [timestep]
"""
import sys
import os
import math
import matplotlib
from matplotlib import pylab as p
import numpy
from numpy.fft import fft
import tables
from matplotlib import ticker
from pylab import *

#
# Declare the number of modes and names
#
nstxDir = "/scr2_oxygen/kindig/austin_nstx/"
outDir = nstxDir + "visitData/"
dataDir = nstxDir + "NSTXHDF5Files/"
maskDir = nstxDir + "threshold/"
nmodes = 4
time = int(sys.argv[1])
nameR = "YeeElecFieldR"
nameI = "YeeElecFieldI"
nameM = "complexNSTX_mode"
nameD = "data_%02d" % time
Mask = "complexNSTX_threshold_1.h5"

outFileName = "%s%s.h5" % (outDir, nameD)

import os.path
if os.path.exists(outFileName):
    import sys
    sys.exit("data file exists: %s" % outFileName)

if not os.path.exists(dataDir):
    import sys
    sys.exit("dataDir doesn't exist" % dataDir)

#
# Get filename for plotting
#
fileModeR = []
fileModeI = []
for i in range(1, nmodes+1):
   fileModeR.append("%s%s%d_%s_%d.h5" % (dataDir, nameM, i, nameR, time))
   fileModeI.append("%s%s%d_%s_%d.h5" % (dataDir, nameM, i, nameI, time))

# 
# Get handle to variable that is being plotted
#
modeHandleR = []
modeHandleI = []
start = 1
end = nmodes + 1
for i in range(nmodes):
   modeHandleR.append(tables.openFile(fileModeR[i],"r"))
   modeHandleI.append(tables.openFile(fileModeI[i],"r"))

ggg = modeHandleR[0].root.globalGridGlobal
z = ggg.z
nz = z.shape[0]

r = ggg.r 
nr = r.shape[0]

#
# Get field values
#
fldR = []
fldI = []
for i in range(nmodes):
  fldR.append(modeHandleR[i].getNode("/"+nameR))
  fldI.append(modeHandleI[i].getNode("/"+nameI))

#
# Declare local field arrays
# 
fieldR_z = []
fieldR_r = []
fieldR_f = []
for i in range(nmodes):
  fieldR_z.append(numpy.zeros((nz,nr)))
  fieldR_r.append(numpy.zeros((nz,nr)))
  fieldR_f.append(numpy.zeros((nz,nr)))

fieldI_z = []
fieldI_r = []
fieldI_f = []
for i in range(nmodes):
  fieldI_z.append(numpy.zeros((nz,nr)))
  fieldI_r.append(numpy.zeros((nz,nr)))
  fieldI_f.append(numpy.zeros((nz,nr)))

#
# Pull out the part of array that we are interested in
#
for i in range(nmodes):
  fieldR_z[i][:,:] = fldR[i][:,:,0]
  fieldR_r[i][:,:] = fldR[i][:,:,1]
  fieldR_f[i][:,:] = fldR[i][:,:,2]

  fieldI_z[i][:,:] = fldI[i][:,:,0]
  fieldI_r[i][:,:] = fldI[i][:,:,1]
  fieldI_f[i][:,:] = fldI[i][:,:,2]

#
# Construct each modes dependence on phi
#
dphi = (2.0*math.pi)/100
phis = numpy.arange(0.0,2.0*math.pi+0.001,dphi)
nphi = phis.shape[0]

#
# Create array for phi component of different modes
# 
pcos = []
psin = []
for i in range(nmodes):
  pcos.append(numpy.cos(1.0*i*(phis)))
  psin.append(numpy.sin(1.0*i*(phis)))

totalMode_z = numpy.zeros((nz,nr,nphi))
totalMode_r = numpy.zeros((nz,nr,nphi))
totalMode_f = numpy.zeros((nz,nr,nphi))

for i in range(nphi):
   for j in range(nmodes):
      totalMode_z[:,:,i] = totalMode_z[:,:,i] + (pcos[j][i]*fieldR_z[j][:,:] - psin[j][i]*fieldI_z[j][:,:])
      totalMode_r[:,:,i] = totalMode_r[:,:,i] + (pcos[j][i]*fieldR_r[j][:,:] - psin[j][i]*fieldI_r[j][:,:])
      totalMode_f[:,:,i] = totalMode_f[:,:,i] + (pcos[j][i]*fieldR_f[j][:,:] - psin[j][i]*fieldI_f[j][:,:])

mysoln = numpy.zeros((nr,nphi,nz,3))
for k in range(nz):
   for j in range(nphi):
      for i in range(nr):
         mysoln[i,j,k,0] = totalMode_z[k,i,j]
         mysoln[i,j,k,1] = totalMode_r[k,i,j]
         mysoln[i,j,k,2] = totalMode_f[k,i,j]

f = tables.openFile(outFileName,mode='w',title=outFileName)

root = f.getNode("/")
root._f_delAttr("VERSION")
root._f_delAttr("TITLE")
root._f_delAttr("CLASS")
root._f_delAttr("PYTABLES_FORMAT_VERSION")
root._f_setAttr("Description","(Z,R,Phi) data generated from VORPAL")

solnGroup = f.createGroup(f.root,"solution")
solnGroup._f_delAttr("VERSION")
solnGroup._f_delAttr("TITLE")
solnGroup._f_delAttr("CLASS")
solnGroup._f_setAttr("step_number",time)
solnGroup._f_setAttr("time",time)
solnGroup._f_setAttr("time_units","s")

f.createArray("/solution","field",mysoln,"V/m")
fieldNode = f.getNode("/solution/field") 
fieldNode._f_delAttr("CLASS")
fieldNode._f_delAttr("FLAVOR")
fieldNode._f_delAttr("VERSION")
fieldNode._f_delAttr("TITLE")
fieldNode._f_setAttr("units","V/m")
fieldNode._f_setAttr("vsMesh","/cartGrid")
fieldNode._f_setAttr("vsType","variable")

#
# Close the files that have been opened
#
for i in range(nmodes):
   modeHandleR[i].close()
   modeHandleI[i].close()
f.close()
