from visit import *

def getNSTXDataContour(nstx, name, min, max):
    print 'Applying contour'
    print nstx.nstxDatabase
    
    nl = nstx.nLevels/2
    r1 = [( 255 / nl ) * (i+1) for i in range(nl)]
    r2 = [255 for i in range(nl)]
    b1 = [0 for i in range(nl)]
    b2 = [( 255 / nl ) * (i+1) for i in range(nl)]

    r = r1 + r2
    g = [( 255 / (nl*2)) * i + i for i in range(nl * 2)]
    b = b1 + b2

    # Opacities
    o1 = [255 for i in range(nl)]
    o2 =   [4 for i in range(nl)]
    o = o2 + o1
    print len(r), len(g), len(b), len(o)
    
    OpenDatabase(nstx.nstxDatabase + " database", 0)
    AddPlot("Contour", name, 0, 0)
    ContourAtts = ContourAttributes()
    ContourAtts.legendFlag = 1
    ContourAtts.colorType = ContourAtts.ColorByColorTable
    ContourAtts.colorTableName = nstx.dataColorTable
    ContourAtts.invertColorTable = 0
    ContourAtts.contourNLevels = nstx.nLevels
    ContourAtts.minFlag = nstx.calcminmax
    ContourAtts.maxFlag = nstx.calcminmax
    ContourAtts.min = min
    ContourAtts.max = max

    # Set the colors
    ContourAtts.colorType = ContourAtts.ColorByMultipleColors
    for i in range(nstx.nLevels): 
      ContourAtts.SetMultiColor(i, r[i], g[i], b[i], o[i])

    SetPlotOptions(ContourAtts) 
  
def getNSTXDataPC(nstx, name, min, max, legendFlag, opacity):
    OpenDatabase(nstx.nstxDatabase + " database", 0)
    AddPlot("Pseudocolor", name, 0, 0)
    WPCAtts = PseudocolorAttributes()
    WPCAtts.legendFlag = legendFlag
    WPCAtts.lightingFlag = 0
    WPCAtts.minFlag = nstx.calcminmax
    WPCAtts.maxFlag = nstx.calcminmax
    WPCAtts.min = min
    WPCAtts.max = max
    WPCAtts.opacity = opacity
    WPCAtts.colorTableName = nstx.dataColorTable
    SetPlotOptions(WPCAtts) 

def getElevate(scaledMagName):
    AddOperator("Elevate", 0)
    EA = ElevateAttributes()
    EA.variable = scaledMagName
    SetOperatorOptions(EA, 0)

def getTransform():
    """
    Visit rotates a slice that uses the operator Elevate. This unrotates it.
    """
    AddOperator("Transform", 0)
    TA = TransformAttributes()
    TA.doRotate = 0
    TA.rotateOrigin = (0, 0, 0)
    TA.rotateAxis = (0, 0, 1)
    TA.rotateAmount = 0
    TA.rotateType =     TA.Deg  # Deg, Rad
    TA.doScale = 0
    TA.scaleOrigin = (0, 0, 0)
    TA.scaleX = 1
    TA.scaleY = 1
    TA.scaleZ = 1
    TA.doTranslate = 0
    TA.translateX = 0
    TA.translateY = 0
    TA.translateZ = 0
    TA.transformType =   TA.Linear  # Similarity, Coordinate, Linear
    TA.inputCoordSys =   TA.Cartesian  # Cartesian, Cylindrical, Spherical
    TA.outputCoordSys =  TA.Spherical  # Cartesian, Cylindrical, Spherical 
    TA.m00 = 1
    TA.m01 = 0
    TA.m02 = 0
    TA.m03 = 0
    TA.m10 = 0
    TA.m11 = 0  # From 1
    TA.m12 = 1  # From 0
    TA.m13 = 0
    TA.m20 = 0
    TA.m21 = 1  # From 0
    TA.m22 = 0  # From 1
    TA.m23 = 0
    TA.m30 = 0
    TA.m31 = 0
    TA.m32 = 0
    TA.m33 = 1
    TA.invertLinearTransform = 0
    TA.vectorTransformMethod =     TA.AsDirection  # None, AsPoint, AsDisplacement, AsDirection
    TA.transformVectors = 1
    SetOperatorOptions(TA, 0)
    
def getClip():
    AddOperator("Clip", 0)
    ClipAtts = ClipAttributes()
    ClipAtts.plane1Status = 0
    ClipAtts.plane2Status = 0
    ClipAtts.plane3Status = 1
    ClipAtts.plane1Normal = (0.2, 1, 0)
    ClipAtts.plane2Normal = (0.5, -0.5, 0)
    ClipAtts.plane3Normal = (0, 0, -1)
    ClipAtts.planeInverse = 1
    SetOperatorOptions(ClipAtts, 0)

def getSlice(axis, p2d = 0):
    AddOperator("Slice", 0)
    SliceAtts = SliceAttributes()
    if axis == "Y" or axis == "y":
      SliceAtts.axisType = SliceAtts.YAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    if axis == "Z" or axis == "z":
      SliceAtts.axisType = SliceAtts.ZAxis  # XAxis, YAxis, ZAxis, Arbitrary, ThetaPhi
    SliceAtts.upAxis = (0, 0, 1)
    SliceAtts.project2d = p2d
    SliceAtts.interactive = 0
    SliceAtts.meshName = "cartGrid"
    SetOperatorOptions(SliceAtts, 0)  

def getThreshold(name):
    AddOperator("Threshold", 0)
    ThresholdAtts = ThresholdAttributes()
    ThresholdAtts.listedVarNames = ("threshold")
    ThresholdAtts.zonePortions = (1)
    ThresholdAtts.lowerBounds = (1)
    ThresholdAtts.upperBounds = (1)
    ThresholdAtts.defaultVarName = name
    ThresholdAtts.defaultVarIsScalar = 1
    SetOperatorOptions(ThresholdAtts, 0)

def getNSTXDataMinMax(name):
#     OpenDatabase(nstx.nstxDatabase + " database", 0)
    """
    Find the min and max for all 
    @param name The field name to AddPlot()
    """
    AddPlot("Contour", name, 0, 0)
    DrawPlots()
    SetQueryFloatFormat("%g")
    Query("MinMax", 1, 0, "default")
    aa = GetQueryOutputString()
    mm = aa.split()
    index = 0
  
    for c in mm:
      if c == "Min": min = float(mm[index+2])
      if c == "Max": max = float(mm[index+2])
      index += 1
  
    if -1 * min > max: max = -1 * min
    if -1 * min < max: min = -1 * max
    min = min / 2.
    max = max / 2.
  
    DeleteAllPlots()
    return (min, max)
  
def getNSTXWall( nstx, wallFile, start = 0, end = 8):
    """
    @param wallFile Full path to the file containing the wall section data
    @param start Starting section of wall
    @param end Ending section of wall
    """
    print 'Getting Wall'
    OpenDatabase(wallFile)
    for i in range(start, end):
      name = "WallSurface%02d" % i
      mesh = "cell_constant(WallSection_%02d, -0.5)" % i
      DefineScalarExpression(name, mesh)
      AddPlot("Pseudocolor", name, 0, 0)
      SetActivePlots(i)
      PCAtts = PseudocolorAttributes()
      PCAtts.colorTableName = nstx.wallColorTable
      PCAtts.minFlag = 1
      PCAtts.maxFlag = 1
      PCAtts.min = -1
      PCAtts.max = 0
      PCAtts.legendFlag = 0
      SetPlotOptions(PCAtts)
  
def addAntenna(nstx, antennaFile):
    print 'Getting Antenna'
    OpenDatabase(antennaFile)
    for i in range(12):
        name = "Antenna_%02d" % i
        ccn  = "Ant%02d" % i
        mesh = "cell_constant(%s, -0.5)" % name
        result = DefineScalarExpression(ccn, mesh)
  
        AddPlot("Pseudocolor", ccn, 0, 0)
        PCAtts = PseudocolorAttributes()
        PCAtts.legendFlag = 0
        SetPlotOptions(PCAtts)
  
def setLighting(nstx, enabled = [], brightness = .6):
    """
    @param enabled list in range 0..8 of lights to enable
    @param brightness 0..1
    """
    enabled = [0, 1, 2, 3]
    enabled = [0, 1]
    direction = [(0.5, 0.5, 0.25), (-0.5, 0.5, 0.25), (0, 0.2, -1), (0, 0.2, 1)]
    index = 0
    for lNum in enabled:
        light = LightAttributes()
        light.enabledFlag = 1
        light.type = light.Object
        light.color = (255, 255, 255, 255)
        light.direction = direction[index]
        light.brightness = brightness
        SetLight(lNum, light)
        index += 1
  
def SetDataAttributes( nstx, ctnName):
    """
    @param colortablename From visit
    """
    ContourAtts = ContourAttributes()
    SetPlotOptions(ContourAtts)
  
def setAnnotation( nstx ):
    AnnotationAtts = AnnotationAttributes()
    AnnotationAtts.databaseInfoFlag = 0
    AnnotationAtts.backgroundColor = (18, 18, 18, 255)
    AnnotationAtts.foregroundColor = (255, 255, 255, 255)
    AnnotationAtts.axes3D.visible = nstx.axesvisible
    AnnotationAtts.axes3D.triadFlag = 1
    AnnotationAtts.axes3D.bboxFlag = 0
    SetAnnotationAttributes(AnnotationAtts)

def setRenderingAttributes():
    RA = RenderingAttributes()
    RA.scalableActivationMode = RA.Never
    SetRenderingAttributes(RA)

#def SetSaveWindowAttributes():
#    swa = SetSaveWindowAttributes()
#    swa.screenCapture = 1
#    SetSaveWindowOptions(swa)
