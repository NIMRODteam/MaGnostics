#!/usr/bin/env python

"""
Calculating the density perturbation
======================================

n_{pert}(x,t) = n(x,t) - n_o(x)

The question is what to do with n_o.

In experimental BES analysis, n_o is calculated by taking a time average.  Thus
in simulation, a time-average is also best. However, if the underlying n_o is
slowly evolving (e.g. if the underlying boundary conditions and sources do not
lead to a steady state) this can lead to incorrect low frequencies in the
analysis.  To fix, it is possible to use a time-dependent toroidal average,
n_o(x,t), instead.

Because the details of how the calculation of n_o requires detailed
understanding of how the code was run, it is in the code section.  Because it
also requires understanding the issues in the calculation of synthetic
diagnostics and is a generic code issue, it is kept in MaGnostics.

"""
import numpy as np
import tables
import glob
import re
import optparse

def dumpH5(fn2, neMean, neDiff, nneDiff, stepNumber) :
    """
    Write the averages in VizSchema-compliant HDF5 format.
    stepNumber is a string despite the variable name
    """
    h5 = tables.open_file("dsyn." + stepNumber + ".vsh5", mode="w", title="Synthetic BES results")
    print("Generating file ", "dsyn." + stepNumber + ".vsh5")
    # Store grid in the hdf5 file
    h5Group = h5.create_group(h5.root, "globalGridGlobal")
    dset = h5.create_array(h5Group, "RZ", fn2.root.globalGridGlobal.RZ[:,:,:])
    dset.set_attr("vsKind", "structured")
    dset.set_attr("vsType", "mesh")
    dset.set_attr("vsAxisLabels", "R,Z")
    dset.set_attr("vsNumSpatialDims", 2)
    dset.set_attr("vsLabels", "r,z")
    dset.set_attr("vsNumCells", [len(fn2.root.globalGridGlobal.RZ), len(fn2.root.globalGridGlobal.RZ[0])])
    dset.set_attr("vsStartCell", [0, 0])
    h5Group = h5.create_group(h5.root, "globalGridGlobalLimits")
    h5.set_node_attr(h5Group, "vsKind", "Cartesian")
    h5.set_node_attr(h5Group, "vsType", "limits")
    # Store mean density data
    dset = h5.create_array(h5.root, "densityMean", neMean)
    dset.set_attr("vsType", "variable")
    dset.set_attr("vsNumSpatialDims", 2)
    dset.set_attr("vsLabels", "ne")
    dset.set_attr("vsCentering", "none")
    dset.set_attr("vsLimits", "/globalGridGlobalLimits")
    dset.set_attr("vsTimeGroup", "time")
    dset.set_attr("vsMesh", "/globalGridGlobal/RZ")
    # Store perturbations
    dset = h5.create_array(h5.root, "densityPerturbation", neDiff)
    dset.set_attr("vsType", "variable")
    dset.set_attr("vsNumSpatialDims", 2)
    dset.set_attr("vsLabels", "dne")
    dset.set_attr("vsCentering", "none")
    dset.set_attr("vsLimits", "/globalGridGlobalLimits")
    dset.set_attr("vsTimeGroup", "time")
    dset.set_attr("vsMesh", "/globalGridGlobal/RZ")
    # Store ratio 
    dset = h5.create_array(h5.root, "normalizedPerturbation", nneDiff[:,:,:])
    dset.set_attr("vsType", "variable")
    dset.set_attr("vsNumSpatialDims", 2)
    dset.set_attr("vsLabels", "dnenorm")
    dset.set_attr("vsCentering", "none")
    dset.set_attr("vsLimits", "/globalGridGlobalLimits")
    dset.set_attr("vsTimeGroup", "time")
    dset.set_attr("vsMesh", "/globalGridGlobal/RZ")
    # Store time
    h5GroupTime = h5.create_group(h5.root, "time")
    h5.set_node_attr(h5GroupTime, "vsType", "time")
    h5.set_node_attr(h5GroupTime, "vsStep", fn2.get_node_attr(fn2.root.time, "vsStep"))
    h5.set_node_attr(h5GroupTime, "vsTime", fn2.get_node_attr(fn2.root.time, "vsTime"))
    h5.close()

def phiAverage(phiDir, synFN, nAverage):
  '''
    Average over toroidal angle

  Collection of hdf5 files with densitites at different phi locations are
  expected to be organized in separate directories for different phi angles.
  The following file/directory structure is expected:

      phiDir.M/syn_file_name.step_number.vsh5

  M is the numbers from 00 to nAverage.

  The syn_file_name is the base of hdf5 files with the synthetic diagnostic
  generated using the NIM-eval.py script. The default value for this variable 
  is set to sym but it can be changed using the option -s.  

  '''

  # density is averaged using sampling in phi
  dirNames = [ phiDir + '.' + '%02i'%i + '/' for i in range(nAverage+1) ]
  fileNames = sorted(glob.glob(dirNames[0] + synFN + "*.vsh5"))
  nt = np.zeros([8,8,1])
  neMean = np.zeros([len(fileNames), 8, 8, 1])
  neDiff = np.zeros([len(fileNames), 8, 8, 1])

  it = 0
  for fs in fileNames :
    ff = [ d + fs.split('/')[-1] for d in dirNames ] 
    ne = np.zeros([len(dirNames),8,8,1])
    ig = 0
    for fname in ff :
      f = tables.open_file(fname, 'r')
      for ir in range(8):
        for iz in range(8):      
          ne[ig,ir,iz,0] = f.root.density[ir,iz,0]
      f.close()
      ig += 1  
    neMean[it,:,:,:] = np.mean(ne, axis=0)
    neDiff[it,:,:,:] = ne[0] - neMean[it]
    it += 1
    #if (fs[-11:]=='159595.vsh5') :
    neAdjustment = np.mean(neDiff, axis=0)
    #print(neAdjustment)
    #print(np.mean(neDiff[:]-neAdjustment, axis=0))
    #print(np.mean(neDiff[:]/(neMean[:]), axis=0))
    #print(np.mean((neDiff[:]-neAdjustment)/(neMean[:]-neAdjustment), axis=0))
    it = 0
    for fs in fileNames :  
      f = tables.open_file(dirNames[0] + fs.split('/')[-1], 'r')  
      dumpH5(f, neMean[it]-neAdjustment, neDiff[it]-neAdjustment, (neDiff[it]-neAdjustment)/np.mean(neMean, axis=0), re.findall(r'\d+', fs.split('/')[-1])[0])
      f.close()
      it += 1
    return

def timeAverage(synDir,synFN):
  # density is averaged over time
  sFiles = sorted(glob.glob(synDir + "/" + synFN + "*.vsh5"))
  f = [ tables.open_file(s, 'r') for s in sFiles ]
  ne = np.array([ fo.root.density for fo in f ])
  neMean = ne.mean(axis=0)
  neDiff = ne - neMean
  for i in range(len(sFiles)) :
    dumpH5(f[i], neMean, neDiff[i], neDiff[i]/neMean, re.findall(r'\d+', sFiles[i])[0])
    f[i].close()
  return
