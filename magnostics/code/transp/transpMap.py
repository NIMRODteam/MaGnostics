# 
# EINJA(IB) = full injection energy of beam (eV)
# PINJA(IB) = power injected by beam (WATTS)
# TBONA(IB) = beam turn on time (seconds)
# TBOFFA(IB)= beam turn off time (seconds)
# 
# PDELTA(IB)= (no longer used, retained for namelist compatibility)
# 
# RTCENA(IB)= beam tangency radius (cm - dist to tokamak centerline)
# FFULLA(IB)= ptcl fraction of beam neutrals which are at full beam 
#             energy
# FHALFA(IB)= fraction of beam neutrals injected at half energy
#             (1.0-FFULLA(IB)-FHALFA(IB) = fraction at 1/3 energy)
# 
# XLBTNA(IB)= distance, ion source to beam tangency radius
# XYBSCA(IB)= elevation of beam ion source above/below midplane
# NLCO(IB)  = .TRUE. if the beam is CO-injecting with the plasma
#             current.
# XBZETA(IB)= toroidal angle of the beam source in an (R,zeta,Z)
#             right handed coordinate system (deg).
# 
# If the following are defaulted, the scalar controls NBSHAP, FOCLR,
# FOCLZ, DIVR, and DIVZ are applied to ALL beams:
# 
# NBSHAPA(IB) -- ion source grid shape of each beam
#                1 = rectangle, 2 = circular
# BMWIDRA(IB) -- source grid radius (if circular) or half-width 
#                (if rectangular)
# BMWIDZA(IB) -- source grid half-height (if rectangular)
#                (ignored if NBSHAPA(IB)=2).
# 
# FOCLRA(IB) -- horizontal focal length (cm) of each beam
# FOCLZA(IB) -- vertical focal length (cm) of each beam
#               (only used if NBSHAPA(IB)=1)
# 
# DIVRA(IB) -- horizontal divergence (radians) of each beam
# DIVZA(IB) -- vertical divergence (radians) of each beam
#               (only used if NBSHAPA(IB)=1)
# 
# 
#   Required 1st aperture into vacuum vessel:
#   -----------------------------------------
# XLBAPA(IB)= distance, ion source to beam aperture into Vacuum Vessel
#   (this is the distance of the beam centerline from the ion source to
#   the plane containing the aperture, which is assumed to be perpendicular
#   to the beam centerline)
# XYBAPA(IB)= elevation of beam centerline above/below plasma midplane
#   when it passes through the aperture
# NBAPSHA(IB)= aperture shape 1=rectangular (default)  2=circular
# RAPEDGA(IB)= aperture horiz half-width or radius if aperture is
#    circular (default = scalar REDGE, see "fixed_controls")
# XZPEDGA(IB)= aperture half-height, if aperture is rectangular
#    (default = scalar XZEDGE, see "fixed_controls")
# 
# New DMC Aug 2009:  Aperture offsets can be defined.  Default is that the
#   offsets are zero, i.e. by default the apertures are centered around the
#   beam centerline.  Offset feature added per request of DIII-D scientists.
# XRAPOFFA(IB) radial offset of aperture relative to beam centerline: a 
#   positive value shifts the aperture out in major radius, favoring beam
#   neutral trajectories with tangency radius greater than the beam 
#   centerline tangency radius (RTCENA(IB)).
# XZAPOFFA(IB) vertical offset of aperature relative to beam centerline.
#   A positive value shifts the aperture upward relative to the beam 
#   centerline.
# 
#   Optional 2nd aperture into vacuum vessel:
#   -----------------------------------------
# XLBAPA2(IB)= distance, ion source to 2nd aperture into Vacuum Vessel
#    (default 0.0 = NO SUCH APERTURE)
# NBAPSH2(IB)= aperture shape:  default 0= NO SUCH APERTURE;
#    1=rectangular 2=circular
# RAPEDG2(IB)= aperture horiz half-width or radius if aperture is
#    circular (default = scalar REDGE, see "fixed_controls")
# XZPEDG2(IB)= aperture half-height, if aperture is rectangular
#    (default = scalar XZEDGE, see "fixed_controls")
# 
# New DMC Aug 2009:
# XRAPOFF2(IB) radial offset of 2nd aperture (definition analogous to 
#    XRAPOFFA(IB) for the main aperture).
# XZAPOFF2(IB) vertical offset of 2nd aperture (definition analogous to 
#    XZAPOFFA(IB) for the main aperture).
# 
# Note BOTH NBAPSH2 AND XLBAPA2 must be set to obtain modeling of 
# a 2nd aperture in the Monte Carlo deposition routine.
# 
# #    PDELTA(1) =  1.000000E-03

NLCO(2) = .T
 EINJA(2) =  0.000000E+00
 PINJA(2) =  0.000000E+00
 !TBONA(2) =  4.112500E-01
 TBONA(2) =  1.010
 TBOFFA(2) =  5.291250E+00
 ABEAMA(2) =  2.000000E+00
 XZBEAMA(2) =  1.000000E+00
 PDELTA(2) =  1.000000E-03
 RTCENA(2) =  1.146000E+02
 XLBAPA(2) =  1.861000E+02
 XLBTNA(2) =  8.028000E+02
 FFULLA(2) =  0.000000E+00
 FHALFA(2) =  0.000000E+00
 XYBAPA(2) =  0.000000E+00
 XYBSCA(2) =  0.000000E+00


