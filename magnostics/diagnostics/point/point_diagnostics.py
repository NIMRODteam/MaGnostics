# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 13:37:34 2014

@author: hebert
"""

from diagnostic import diagnostic

class point_te(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="te"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 

class point_ti(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="ti"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 


class point_pe(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="pe"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z)
        
class point_pi(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="pi"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 

class point_nd(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="nd"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 

class point_B(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="B"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return self.signal

class point_J(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="J"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 
        
class point_V(diagnostic):
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        self.var="V"
        
    def get_signal(self,model):
        self.signal=model.get_data(self.var,self.x,self.y,self.z) 
        return model.get_data(self.var,self.x,self.y,self.z) 
        
