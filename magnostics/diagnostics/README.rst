

The organization of this is by type of diagnostics.

Roughly:

magnetics
---------

Rogowski coil, Mirnov coils, etc.

point
-----

No diagnostic is truly a point measurement, but these are in a class that is
close


line_measurements
-----------------
  
These are line integrated diagnostics:
   interferometer, soft X-ray, etc.

PSF
---

These are measurements that are relatively local but require having a
probability source function as part of its weighted measurement.

Examples: BES, ECEI
