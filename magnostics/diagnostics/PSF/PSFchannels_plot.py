#!/usr/bin/env python
"""
Plot sing PSF function
"""
import os
import sys
import h5py
import optparse
import matplotlib.pyplot as plt
import numpy as np

# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
file_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(file_dir)
diagnostics_dir = os.path.dirname(file_dir)
sys.path.append(diagnostics_dir)
magnostics_dir = os.path.dirname(diagnostics_dir)
machine_dir = os.path.join(magnostics_dir, "machine")
sys.path.append(machine_dir)

import simpleWall
import PSFplot
import PSF
from PSFchannels import psfChannelArrays


class psfChannelArraysPlot(psfChannelArrays):
    """
    Plot the psfChannelArrays
    """

    def plot(self, wallFile=None, noShow=False, plotGrid=False,
             showFile=False):
        """
          Plot the centers and, if the psf data has been configured,
          the contour plots of the PSF functions.
          plotGrid:  Show grid instead of contours
          showFile:  Annotate centers with filenames instead of plotting psf
          """
        for arrayName in self.arrayNames:
            array = getattr(self, arrayName)
            fig, ax = plt.subplots()
            fig = plt.scatter(array.RZpts[0], array.RZpts[1])
            NR = array.RZpts[0].shape[0]
            NZ = array.RZpts[1].shape[1]
            k = -1
            if hasattr(array, "psfArray"):
                for j in range(NZ):
                    for i in range(NR):
                        k += 1
                        if showFile:
                            ax.annotate(
                                os.path.basename(array.psfFileList[k]),
                                (array.RZpts[0, i, j], array.RZpts[1, i, j]),
                            )
                        elif plotGrid:
                            # Use plotGrid function without inheriting
                            PSFplot.PSFplot.plotGrid(
                                array.psfArray[i, j], size=0.1, noShow=True
                            )
                        else:
                            PSFplot.PSFplot.plotContour(
                                array.psfArray[i, j], noShow=True, ax=ax
                            )
            if wallFile:
                sw = simpleWall.simpleWall(wallFile)
                plt.plot(sw.Rwall, sw.Zwall, linestyle="-", color="blue")
        plt.axis("equal")
        plt.xlabel("R (m)")
        plt.ylabel("Z (m)")
        wallTitle = "PSF Channel Points"
        plt.title(wallTitle)
        if not noShow:
            plt.show()
        return fig


datalist = []


def returnname(name):
    if name not in datalist:
        return name
    else:
        return None


def restore(h5handle=None, h5FileName="PSFchannel.h5"):
    """
  This is a copy of the PSFchannels restore only with PSFchannels ->
  PSFchannelsPlot

  This is one of those things that normal inheritance or metaprogramming
  doesn't solve and some of the other tricks don't seem to work.
  """
    close = False
    if not h5handle:
        h5handle = h5py.File(h5FileName, mode="r")
        close = True
    hf = h5handle

    initDict = {}

    arrayNames = hf.attrs.get("arrayNames")
    initDict["arrayNames"] = arrayNames
    # Most of these will get put into info
    for atts in hf.keys():
        hfatts = hf.attrs.get(atts)
        if hfatts:
            initDict[atts] = hfatts

    for aname in arrayNames:
        agrp = hf.get(aname)
        ds = agrp.get("RZpts")[:, :, :]
        NR = agrp.attrs.get("NR")
        NZ = agrp.attrs.get("NZ")
        initDict[aname] = {}
        initDict[aname]["RZpts"] = ds
        initDict[aname]["NR"] = NR
        initDict[aname]["NZ"] = NZ

        while 1:
            item = agrp.visit(returnname)
            if item is None:
                break
            datalist.append(item)
            if item not in arrayNames:
                hfitem = agrp.get(item)
                if isinstance(hfitem, h5py.Dataset):
                    continue
                if "PSFs" not in initDict[aname]:
                    initDict[aname]["PSFs"] = {}
                initDict[aname]["PSFs"][item] = PSF.restore(hfitem)

    newpsf = psfChannelArraysPlot(None, restoreDict=initDict)

    if close:
        h5handle.close()
    return newpsf


def main():
    parser = optparse.OptionParser(usage="%prog [options]")
    parser.add_option(
        "-w",
        "--wall",
        dest="wallFile",
        default=None,
        help="Wall file to include in plots",
    )
    parser.add_option(
        "-c",
        "--configFile",
        dest="configFile",
        default=None,
        help="File with shot data",
    ),
    parser.add_option("-p",
                      "--psfFile",
                      dest="psfFile",
                      default=None,
                      help="File with psf data"),
    parser.add_option("-d",
                      "--psfDir",
                      dest="psfDir",
                      default=None,
                      help="File with psf data"),
    parser.add_option(
        "-C",
        "--recenter",
        dest="recenter",
        action="store_true",
        help="Recenter the PSF data to that from the configFile",
    ),
    parser.add_option(
        "-g",
        "--plotGrid",
        action="store_true",
        dest="plotGrid",
        help="Plot grid of psf instead of contour",
    ),
    parser.add_option(
        "-f",
        "--showFile",
        action="store_true",
        dest="showFile",
        help="Show psf file name at each point instead of grid or countour",
    ),
    parser.add_option(
        "-D",
        "--dump",
        dest="dump",
        action="store_true",
        help="Dump the data instead of plotting",
    ),
    parser.add_option(
        "-r",
        "--restore",
        dest="h5file",
        default=None,
        help="Restore from a given file"
    ),
    parser.add_option(
        "-R",
        "--Rshift",
        dest="Rshift",
        default=0.0,
        help="Shift all PSF grids in R"
    ),
    parser.add_option(
        "-Z",
        "--Zshift",
        dest="Zshift",
        default=0.0,
        help="Shift all PSF grids in Z"
    ),
    options, args = parser.parse_args()

    if not options.h5file:
        if len(args) > 0:
            parser.print_usage()
            return

    if options.configFile:
        if not os.path.exists(options.configFile):
            print("Error: Shot file does not exist, ", options.configFile)
            return

    if options.psfFile:
        options.recenter = True  # Single file always needs recentering
        if not os.path.exists(options.psfFile):
            print("Error: PSF file does not exist, ", options.psfFile)
            return

    if options.psfDir:
        if not os.path.exists(options.psfDir):
            print("Error: PSF directory does not exist, ", options.psfDir)
            return

    if options.h5file:
        if not os.path.exists(options.h5file):
            print("File does not exist: ", options.h5file)
            return
        myPsfArrays = restore(h5FileName=options.h5file)

    else:

        myPsfArrays = psfChannelArraysPlot(configFile=options.configFile)

        if options.psfFile or options.psfDir or myPsfArrays.hasPsfInfo:
            myPsfArrays.addPSF(
                psfFile=options.psfFile,
                psfDir=options.psfDir,
                recenterPSF=options.recenter,
                Rshift=options.Rshift,
                Zshift=options.Zshift,
            )

    if options.dump:
        myPsfArrays.dump()
        return

    myPsfArrays.plot(
        wallFile=options.wallFile, plotGrid=options.plotGrid,
        showFile=options.showFile
    )
    return


if __name__ == "__main__":
    main()
