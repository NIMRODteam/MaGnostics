#!/usr/bin/env python
"""
Plot the set of rzpoints as a scatter plot
"""
import os
import matplotlib.pyplot as plt
import optparse
import yaml
import sys

# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)
machine_dir = os.path.join(os.path.dirname(file_dir), "machine")
sys.path.append(machine_dir)

from rzpoints import diagnosticPoints
import simpleWall


class pointsPlot(diagnosticPoints):
    """
    Plot the diagnostic points
    """

    def plot(self, wallFile=None, noShow=False):
        if wallFile:
            sw = simpleWall.simpleWall(wallFile)
            plt.plot(sw.Rwall, sw.Zwall, linestyle="-", color="blue")
        fig = plt.scatter(self.RZpts[0], self.RZpts[1], color="red")
        plt.axis("equal")
        plt.xlabel("R (m)")
        plt.ylabel("Z (m)")
        wallTitle = "Points"
        plt.title(wallTitle)
        if not noShow:
            plt.show()
        return fig


def main():
    parser = optparse.OptionParser(usage="%prog [options] geomFile")
    parser.add_option(
        "-w",
        "--wall",
        dest="wallFile",
        default=None,
        help="Wall file to include in plots",
    )
    options, args = parser.parse_args()

    if len(args) > 1 or len(args) == 0:
        parser.print_usage()
        return
    else:
        inputFile = args[0]
        if not os.path.exists(inputFile):
            print("Error: file does not exist, ", inputFile)
            return

    with open(inputFile) as filestr:
        inputConf = yaml.load(filestr, Loader=yaml.Loader)

    myrz = pointsPlot(inputConf["array1"])
    myrz.plot(options.wallFile)
    return


if __name__ == "__main__":
    main()
