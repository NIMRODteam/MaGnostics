#!/usr/bin/env python
"""
Plot sing PSF function
"""
import os
import sys
import matplotlib.pyplot as plt

# This import is needed just to allow projection='3d':
from mpl_toolkits.mplot3d import Axes3D
import optparse
import h5py

# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
sys.path.append(os.path.dirname(__file__))

from psf import singlePSF


class PSFplot(singlePSF):
    """
    Class for a single PSF
    """

    def plotContour(self, noShow=False, figNum=None, ax=None):
        """
      Plot psf as a surface plot
      figNum is if this is called multiple times.
      """
        rg, zg = self.grid
        if not figNum:
            figNum = 1
            fig = plt.figure(figNum)
        if not ax:
            ax = fig.add_subplot(111)
        ax.contour(rg, zg, self.values)
        ax.set_xlabel("R")
        ax.set_ylabel("Z")
        rcen = self.centerLoc[0]
        zcen = self.centerLoc[1]
        plt.scatter(rcen, zcen)
        if not noShow:
            plt.show()
        return fig

    def plotSurface(self, noShow=False, figNum=None, ax=None):
        """
      Plot psf as a surface plot
      figNum is if this is called multiple times.
      """
        if not figNum:
            figNum = 1
            fig = plt.figure(figNum)
        if not ax:
            ax = fig.add_subplot(111, projection="3d")
        rg, zg = self.grid
        ax.plot_surface(rg, zg, self.values)
        ax.set_xlabel("R")
        ax.set_ylabel("Z")
        ax.set_zlabel("psf Values")
        if not noShow:
            plt.show()
        return fig

    def plotGrid(self, figNum=1, size=None, noShow=False):
        """
      Plot psf as a surface plot
      figNum is if this is called multiple times.
      """
        rg, zg = self.grid
        fig = plt.scatter(rg, zg, s=size)
        plt.axis("equal")
        plt.xlabel("R (m)")
        plt.ylabel("Z (m)")
        if not noShow:
            plt.show()
        return fig


def restore(h5handle=None, h5FileName="PSF.h5"):
    """
  This is a copy of the PSF restore only with PSF -> PSFplot

  This is one of those things that normal inheritance or metaprogramming
  doesn't solve and some of the other tricks don't seem to work.
  """
    close = False
    if not h5handle:
        h5handle = h5py.File(h5FileName, mode="r")
        close = True
    hf = h5handle

    newpsf = PSFplot()

    newpsf.phi = hf.attrs.get("phi")
    newpsf.channelNumber = hf.attrs.get("channelNumber")
    newpsf.channelArrayLabel = hf.attrs.get("channelArrayLabel")
    newpsf.file = hf.attrs.get("file")
    newpsf.centerLoc = hf.attrs.get("centerLoc")
    # TODO:  Check that VizSchema is right
    rg = hf.get("Rgrid")[:, :]
    zg = hf.get("Zgrid")[:, :]
    newpsf.grid = [rg, zg]
    newpsf.values = hf.get("PSF")[:, :]

    if close:
        h5handle.close()
    return newpsf


def main():
    """
      Plot a single PSF function
    """
    parser = optparse.OptionParser(usage="%prog [options]")
    parser.add_option(
        "-p",
        "--psf",
        dest="psfFile",
        default=None,
        help="PSF file name in yaml or idl sav format",
    )
    parser.add_option(
        "-u",
        "--phi",
        dest="phi",
        default=0.0,
        help="Phi angle to assign to PSF (does nothing)",
        type=float,
    )
    parser.add_option(
        "-c",
        "--plotContour",
        action="store_true",
        dest="plotContour",
        help="Plot grid of psf instead of contour",
    ),
    parser.add_option(
        "-D",
        "--dump",
        dest="dump",
        action="store_true",
        help="Dump the data instead of plotting",
    ),
    parser.add_option(
        "-r", "--restore", dest="h5file", 
        default=None, help="Restore from a given file"
    ),
    options, args = parser.parse_args()

    if len(args) > 0:
        parser.print_usage()
        return

    if options.h5file:
        if not os.path.exists(options.h5file):
            print("File does not exist: ", options.h5file)
            return
        mypsf = restore(h5FileName=options.h5file)
    elif options.psfFile:
        mypsf = PSFplot(psfFileName=options.psfFile, phi=options.phi)
    else:
        mypsf = PSFplot(phi=options.phi)

    if options.dump:
        mypsf.dump()
        return

    if options.plotContour:
        mypsf.plotContour()
    else:
        mypsf.plotSurface()
    return


if __name__ == "__main__":
    main()
