# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 10:16:10 2015

@author: hebert
"""

import numpy

class constant_rphi_rogowski():
    def __init__(self,r,phi,thetas):
        cost=numpy.cos(thetas)
        sint=numpy.sin(thetas)
        self.cost=cost
        self.sint=sint
        self.xlist=numpy.zeros(len(thetas))
        self.ylist=numpy.zeros(len(thetas))
        self.zlist=numpy.zeros(len(thetas))
        self.dtheta=numpy.zeros(len(thetas))
        dthetamag=numpy.zeros(len(thetas))
        
        for i in range(len(thetas)):
            self.xlist[i]=(0.75+r*cost[i])*numpy.cos(phi)
            self.ylist[i]=(0.75+r*cost[i])*numpy.sin(phi)
            self.zlist[i]=r*sint[i]
            if i<len(thetas)-1:
                dthetamag[i]=numpy.abs(r*(thetas[i+1]-thetas[i]))
            else:
                dthetamag[i]=numpy.abs(r*(thetas[0]-thetas[i]))
            self.dthetamag=dthetamag
            self.dtheta=dthetamag*numpy.array([-sint,cost])
            
    def get_signal(self,model):
        for i in range(len(self.xlist)):
            sig=model.get_data("B",self.xnodes[i],self.ynodes[i],self.znodes[i]))/(4*numpy.pi*10**-7)
            