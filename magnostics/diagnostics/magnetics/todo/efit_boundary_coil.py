# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 11:05:39 2015

@author: hebert
"""

from diagnostic import diagnostic
import h5py

class efit_boundary_coil(diagnostic):
    def __init__(self,input_file):
        print "Reading input from file", input_file
        self.h5file=h5py.File(input_file,"r")
        self.contours=self.h5file['grContours'].keys()
        print "Looks like there are", len(self.contours), "contours:", self.contours
        if len(self.contours)>1:
            self.contour_points={}
            for i in self.contours:
                self.contour_points[i]=self.h5file['grContours'][i]['points'].value
                
        else:
            self.contour_points=self.h5file['grContours'][self.contours]['points'].value