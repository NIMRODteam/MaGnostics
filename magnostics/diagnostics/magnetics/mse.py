from diagnostic import diagnostic
import numpy
import csv
import netCDF4 as nc
class mse(diagnostic):
    """DIII-D Motional Stark Effect (MSE) diagnostic.  To initialize:
    
    mse(m_file,csv_file)
    
    m_file    :  EFIT m-file
    csv_file  :  Geometric table from MSE config pdf in csv form
    
    """
    def __init__(self,m_file,csv_file):
        """
        Parameters:
        
        m_file    :  EFIT m-file.  Used to grab A parameters and comparison value.  May be done away with
        
        csv_file  :  'Geometric data for old and new systems' table in 'Mse69config - DIII-D.pdf' in CSV form.  Mainly contains evaluation points.
        """        
        
        self.alpha=numpy.array([])
        self.omega=numpy.array([])
        self.theta=numpy.array([])
        self.channel=numpy.array([])
        self.r=numpy.array([])
        self.phid=numpy.array([])
        self.x=numpy.array([])
        self.y=numpy.array([])
        self.z=numpy.array([])
        with open(csv_file,'r') as csvfile:
            reader=csv.DictReader(csvfile)
            for row in reader:
                self.r=numpy.append(self.r,float(row['R']))
                self.phid=numpy.append(self.phid,float(row['phi_d'])*numpy.pi/180)
                self.alpha=numpy.append(self.alpha,float(row['alpha'])*numpy.pi/180)
                self.x=numpy.append(self.x,float(row['x']))
                self.y=numpy.append(self.y,float(row['y']))
                self.z=numpy.append(self.z,float(row['z']))
                self.omega=numpy.append(self.omega,float(row['omega'])*numpy.pi/180)
                self.theta=numpy.append(self.theta,float(row['theta'])*numpy.pi/180)
                self.channel=numpy.append(self.channel,float(row['Channel']))
        dat=nc.Dataset(m_file,"r")
        self.rloc=dat.variables.get('rrgam')[0][:69]
        self.zloc=dat.variables.get('zzgam')[0][:69]
        self.a1gam=dat.variables.get('a1gam')[0][:69]
        self.a2gam=dat.variables.get('a2gam')[0][:69]
        self.a3gam=dat.variables.get('a3gam')[0][:69]
        self.a4gam=dat.variables.get('a4gam')[0][:69]
        self.a5gam=dat.variables.get('a5gam')[0][:69]
        self.a6gam=dat.variables.get('a6gam')[0][:69]
        self.a7gam=dat.variables.get('a7gam')[0][:69]
        self.bdat=numpy.zeros([self.rloc.shape[0],3])
        self.dat=numpy.zeros(self.rloc.shape[0])
        self.tangam=dat.variables.get('tangam')[0][:69]
        self.siggam=dat.variables.get('siggam')[0][:69]
        self.comweight=dat.variables.get('fwtgam')[0][:69]
        self.signal=0
        
    def get_signal(self,model):
        self.bdat=numpy.zeros([len(self.omega),3])
        self.edat=numpy.zeros([len(self.omega),3])
        phi=-numpy.arctan2(self.y,self.x)
        self.dat=numpy.zeros(len(self.omega))
        for i in range(len(self.omega)):
            self.bdat[i]=model.get_data("B",self.x[i],self.y[i],self.z[i])
            self.edat[i]=model.get_data("E",self.x[i],self.y[i],self.z[i])
            
        #Convert to local toroidal, vertical, and radial coordinates
        bt=-self.bdat[:,0]*numpy.sin(phi)+self.bdat[:,1]*numpy.cos(phi)
        bv=self.bdat[:,2]
        br=self.bdat[:,0]*numpy.cos(phi)+self.bdat[:,1]*numpy.sin(phi)
        #Convert to local toroidal, vertical, and radial coordinates
        et=-self.edat[:,0]*numpy.sin(phi)+self.edat[:,1]*numpy.cos(phi)
        ev=self.edat[:,2]
        er=self.edat[:,0]*numpy.cos(phi)+self.edat[:,1]*numpy.sin(phi)        
        #Testing only
        self.bt=bt
        self.bv=bv
        self.br=br
        self.et=et
        self.ev=ev
        self.er=er
        #factors as set in Rice et al., RSI Vol. 70, No. 1
        a1=self.a1gam*bv
        a2=self.a2gam*bt
        a3=self.a3gam*br
        a4=self.a4gam*bv
        a5=self.a5gam*er
        a6=self.a6gam*ev
        a7=self.a7gam*er
        self.dat=(a1+a5)/(a2+a3+a4+a6+a7)
        
        return self.dat
