import numpy
   
class rogowski():
    def __init__(self,xnodes,ynodes,znodes):
        if xnodes.shape[0]!=ynodes.shape[0]:
            raise("xnodes and ynodes have incompatable shapes; all inputs must have same length")
        elif xnodes.shape[0]!=znodes.shape[0]:
            raise("xnodes and znodes have incompatable shapes; all inputs must have same length")
        elif ynodes.shape[0]!=znodes.shape[0]:
            raise("ynodes and znodes have incompatable shapes; all inputs must have same length")
        else:        
            self.xnodes=xnodes
            self.ynodes=ynodes
            self.znodes=znodes
            self.signal=None
        
    def get_signal(self,model):
        sig=0
        length=numpy.zeros([self.xnodes.shape[0]-1])
        sig=numpy.zeros([self.xnodes.shape[0]-1])
        for i in range(1,self.xnodes.shape[0]):
            delta=numpy.array([j[i]-j[i-1] for j in [self.xnodes,self.ynodes,self.znodes]])
            if i!=1:
                length[i-1]=length[i-2]+numpy.linalg.norm(delta)
            else:
                length[i-1]=numpy.linalg.norm(delta)
            sig[i-1]=numpy.dot(model.get_data("B",self.xnodes[i],self.ynodes[i],self.znodes[i]),delta/numpy.linalg.norm(delta))/(4*numpy.pi*10**-7)
###            delta=numpy.array([j[i]-j[i-1] for j in [self.xnodes,self.ynodes,self.znodes]])
###            sig+=numpy.dot(model.get_data("B",self.xnodes[i],self.ynodes[i],self.znodes[i]),delta)/(4*numpy.pi*10**-7)
###        self.signal=sig
        self.length=length
        sig=numpy.trapz(sig,length)  
        return sig
        
    def plot_diagnostic(self,fig):
#        fig=plt.figure()
        
        ax=fig.add_subplot(1,1,1,projection='3d')
        ax.plot(self.xnodes,self.ynodes,self.znodes)
#        plt.show()