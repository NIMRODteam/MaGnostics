#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_psf
--------

Tests for magnostics.diagnostics.PSF module.
"""

import os
import unittest
import magnostics.diagnostics.PSF.psf as psf

parent_dir = os.path.dirname(os.path.dirname(__file__))


class TestPsf(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mypsf = psf.singlePSF()

    @classmethod
    def tearDownClass(cls):
        del cls.mypsf

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_psfIntegrate(self):
        self.assertAlmostEqual(
            5.019716558573792e-4, self.mypsf.integrate(self.mypsf.values)
        )

    def test_readfile_yml(self):
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        confFile = os.path.join(data_dir, "test", "psf.yml")
        self.mypsf.read(confFile)
        self.assertAlmostEqual(
            4.581922320949392e-06, self.mypsf.integrate(self.mypsf.values)
        )

    def test_readfile_sav(self):
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        savFile = os.path.join(data_dir, "diii-d", "psf5.sav")
        self.mypsf.read(savFile)
        self.assertAlmostEqual(
            3.6210724887043654e-4, self.mypsf.integrate(self.mypsf.values)
        )

    def test_dump(self):
        self.mypsf.dump(h5FileName="PSFtest.h5")

    def test_restore(self):
        # Module function
        respsf = psf.restore(h5FileName="PSFtest.h5")
        self.assertAlmostEqual(5.019716558573792e-4,
                               respsf.integrate(respsf.values))
