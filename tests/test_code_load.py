#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rzpoints
----------------------------------

Tests for magnostics.diagnostics.rzpoints module.
"""
import os
import sys
import numpy as np
import unittest
import magnostics.code.code_load as codel

parent_dir = os.path.dirname(os.path.dirname(__file__))
code_dir = os.path.join(parent_dir, "magnostics", "code")
sys.path.append(code_dir)


class TestPsf(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        coord = "rzp"
        dumpFile = os.path.join(data_dir, "test", "dump_exemplar.yml")
        cls.codeeval = codel.codeEval(
            "exemplar", dumpFile, fieldlist="nvbp", coord=coord
        )

    @classmethod
    def tearDownClass(cls):
        del cls.codeeval

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_eval_points(self):
        nr = 300
        nz = 200
        rcenter = 1.7
        zcenter = 0.0
        rlen = 1.6
        zlen = 3.0
        rg = rcenter + rlen * (np.linspace(0, 1, nr) - 0.5)
        zg = zcenter + zlen * (np.linspace(0, 1, nz) - 0.5)
        grid = np.meshgrid(rg, zg, indexing="ij")

        # Convert to complete np array rather than list of arrays
        rzp = np.zeros((3, nr, nz), dtype="double")
        rzp[0, :, :] = grid[0]
        rzp[1, :, :] = grid[1]
        rzp[2, :, :] = 0.0

        field = self.codeeval.eval_field("n", rzp, dmode=0, eq=0)

        self.assertAlmostEqual(field[0, 3, 3], 3.0e19)
