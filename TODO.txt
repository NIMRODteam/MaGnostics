

Scott to merge to master and finish BES in separate branch
Scott to find some examples

CI
      - Get rid of tox.ini
      - Move to .gitlab-ci.yml
            + See abstract_block
            + Need docker image.  Use minimal set (specifically python3)
            + See requirements_dev.rst
            + May want to google `gitlab CI python`
      - Should test:
            + python setup.py install
            + python setup.py test
            + Also test flake8
            + Test coverage
            
Testing
      - Using python.  
      - See CONTRIBUTING.rst

Documentation
      - Sphinx setup
      - Goal is to have a 
            + quick start (user guidance)
                  - Document workflow for a given diagnostic
                    (BES, Soft X-ray, etc.)
            + autodoc (api documentation)
      - Integrate to gitlab wiki through CI
            + Do when MR is approved.  How?  
            (.gitlab-ci.yml combined with repo settings).


Core development
      - Need a line_measurements base class to inherit from rzpoints
            + See PSF diagnostic which is more mature
      - Base class will integrate
      - Soft X-ray to have xray points
      - Need to merge PCI into the line integrations
      - Need testing of line diagnostics (Soft X-Ray)
