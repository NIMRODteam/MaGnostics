

Getting started running the BES diagnostic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The basic steps of doing a BES synthetic diagnostic are:

      1. Specify the channel locations for each array (e.g., DIII-D has two
         arrays:  1 in a grid and 1 in a line)
      2. Obtaining the Point Spread Function (PSF) data for each channel and
         assign them to the array
      3. Evalate the density field from your code over each PSF grid and perform
         the integration for the PSF function

.. todolist::

      1.  Put in equation for PSF integration
      2.  Add in the references
      3.  Add in the images for each command


.. code::

      # Basic commands for configuring arrays
      ./PSFchannelsPlot.py -c ../../data/test/psfConfig.yml 
      ./PSFchannelsPlot.py -c ../../data/test/psfConfig.yml  -w ../../data/diii-d/diiidSimpleWall.yml

.. code::

      # Place PSF grids onto array
      ./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml
      ./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml -g
      ./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml -f

.. code::

      # 1 file in directory -- yaml file
      ./PSFchannelsPlot.py -d ../../data/test/psf_read1 -c ../../data/test/psfConfig.yml

.. code::

      # 1 file in directory -- sav file
      ./PSFchannelsPlot.py -d ../../data/test/psf_read2 -c ../../data/test/psfConfig.yml

.. code::

      # Multiple files in directory -- sav files  (recentered because it's really one file)
      ./PSFchannelsPlot.py -d ../../data/test/psf_read3 -c ../../data/test/psfConfig.yml -f -C

.. code::

      # Multiple files in directory with file listed in shot file -- sav files listed
      # in config file  (recentered because it's really one file)
      ./PSFchannelsPlot.py -c ../../data/test/psf_read4/psfConfig.yml -f -C

.. code::

      # directory full of files that do not need recentering and 
      ./PSFchannelsPlot.py -d ../../data/test/psf_read5/ -c ../../data/test/psfConfigMin.yml
      # Shift the psf files
      ./PSFchannelsPlot.py -d ../../data/test/psf_read5/ -c ../../data/test/psfConfigMin.yml -R 10. -Z -5.



.. code::

      # Plotting just the PSF files themselves
      ./PSFplot.py -p ../../data/test/psf_read2/array1/psf5.sav
      ./PSFplot.py -p ../../data/test/psf_read2/array1/psf5.sav -c
