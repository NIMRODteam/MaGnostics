# Basic commands for configuring arrays
./PSFchannelsPlot.py -c ../../data/test/psfConfig.yml 
./PSFchannelsPlot.py -c ../../data/test/psfConfig.yml  -w ../../data/diii-d/diiidSimpleWall.yml

# Place PSF grids onto array
./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml
./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml -g
./PSFchannelsPlot.py -p ../../data/test/psf.yml -c ../../data/test/psfConfig.yml -f

# 1 file in directory -- yaml file
./PSFchannelsPlot.py -d ../../data/test/psf_read1 -c ../../data/test/psfConfig.yml

# 1 file in directory -- sav file
./PSFchannelsPlot.py -d ../../data/test/psf_read2 -c ../../data/test/psfConfig.yml

# Multiple files in directory -- sav files  (recentered because it's really one file)
./PSFchannelsPlot.py -d ../../data/test/psf_read3 -c ../../data/test/psfConfig.yml -f -C

# Multiple files in directory with file listed in shot file -- sav files listed
# in config file  (recentered because it's really one file)
./PSFchannelsPlot.py -c ../../data/test/psf_read4/psfConfig.yml -f -C

# directory full of files that do not need recentering and 
./PSFchannelsPlot.py -d ../../data/test/psf_read5/ -c ../../data/test/psfConfigMin.yml
# Shift the psf files
./PSFchannelsPlot.py -d ../../data/test/psf_read5/ -c ../../data/test/psfConfigMin.yml -R 10. -Z -5.



# Plotting just the PSF files themselves
./PSFplot.py -p ../../data/test/psf_read2/array1/psf5.sav
./PSFplot.py -p ../../data/test/psf_read2/array1/psf5.sav -c
