.. Master file for MaGnostics documentation
.. $Rev: 7715 $ $Date: 2018-09-03 12:33:00 -0600 (Mon, 03 Sep 2018) $
.. http://www.sphinx-doc.org/en/stable/rest.html

Magnostics Documentation
========================

Quick-start to using MaGnostics
-------------------------------

.. toctree::
   :maxdepth: 1
   :titlesonly:

   bes.rst
   softxray.rst

Code documentation
------------------

.. toctree::
   :maxdepth: 2
   :titlesonly:

   ../magnostics/machine/index.rst
   ../magnostics/diagnostics/index.rst
   ../magnostics/code/index.rst


.. mainhtmldoc: html is standalone
.. latex/pdf: standalone
.. Either case needs refs.
.. only:: maindoc

  .. include:: bibliotop.rst
